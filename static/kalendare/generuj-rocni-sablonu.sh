#!/bin/bash

# skript na vygenerovani dnu kalendare pro cely rok
# uprav cislo na radku 10 pro novy rok
# pak otevri vytvoreny textovy soubor pro novy rok
# smaz prebytecne dny (kazdy mesic je generovan s 31 dny)
# a vysledny text zkopiruj do souboru `script.js`

for mmdd in {01..12}-{01..31}
do
rok=26
#  echo "// let day${rok}${mmdd//-} = formatDate(new Date(\"20${rok}-${mmdd}\"))"  >> 20${rok}.txt
#  echo "// eventDates[day${rok}${mmdd//-}] = ["                                   >> 20${rok}.txt
#  echo "//   'Obsazeno',"                                                         >> 20${rok}.txt
#  echo "// ]"                                                                     >> 20${rok}.txt

# zkraceno na 2 radky:

  echo "// let day${rok}${mmdd//-} = formatDate(new Date(\"20${rok}-${mmdd}\"))"  >> 20${rok}.txt
  echo "// eventDates[day${rok}${mmdd//-}] = ['Volno']"                           >> 20${rok}.txt

done
