+++
date = "2018-10-01"
draft = false
title = "Kontaktní údaje"

+++

<i class="fas fa-map-marked fa-2x"></i>

Valtrova 33  
39165 Bechyně  
Česká Republika

<i class="fas fa-envelope-open fa-2x"></i>

info@uklasternizahrady.cz

<i class="fas fa-phone fa-2x"></i>

+420 728 015 561
