---
date: "2018-10-01T00:00:00+02:00"
publishdate: "2018-10-01T00:00:00+02:00"
lastmod: "2018-10-01T00:00:00+02:00"
draft: false
title: "Dvoulůžkový pokoj - Modrý"
tags: ["okno do zahrady", "1. patro", "dvoulůžkový"]
series: ["Pokoje"]
categories: ["Modrý"]
img: "images/pokoje/modry.jpg"
toc: true
summary: " "
slug: modry
weight: -4
---

Rezervovat můžete e-mailem na info@uklasternizahrady.cz

**1. patro domu, vlastní koupelna,**  28m²  
výhled do klášterní zahrady, na františkánský klášter a kostel, klidná lokalita

**k dispozici** – dle volby manželská&nbsp;postel nebo oddělená lůžka, lůžkoviny, povlečení, osušky, ručníky, toaletní potřeby, dezinfekce, fén, posezení, lampičky, ústřední vytápění, chladnička, wifi

**k dispozici** - snídárna s TV, káva, čaj kdykoliv
