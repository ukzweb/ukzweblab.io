---
date: "2018-10-01T00:00:00+02:00"
publishdate: "2018-10-01T00:00:00+02:00"
lastmod: "2018-10-01T00:00:00+02:00"
draft: false
title: "Snídárna"
tags: ["okno do zahrady", "přízemí"]
categories: ["Snídárna"]
img: "images/pokoje/snidarna.jpg"
toc: true
summary: " "
slug: snidarna
weight: -7
---

V přízemí domu, vchází se přes atrium, k dispozici **všem ubytovaným hostům**

**snídaně** připravíme dle dohody od **8.30 - 10.00 hod**.

**k dispozici** - TV, knihovna, káva, čaj kdykoliv
