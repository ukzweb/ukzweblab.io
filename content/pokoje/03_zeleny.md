---
date: "2018-10-01T00:00:00+02:00"
publishdate: "2018-10-01T00:00:00+02:00"
lastmod: "2018-10-01T00:00:00+02:00"
draft: false
title: "Dvoulůžkový pokoj - Zelený"
tags: ["okno do zahrady", "1. patro", "dvoulůžkový"]
series: ["Pokoje"]
categories: ["Zelený"]
img: "images/pokoje/zeleny.jpg"
toc: true
summary: " "
slug: zeleny
weight: -3
---


Rezervovat můžete e-mailem na info@uklasternizahrady.cz

**1. patro domu, vlastní koupelna,** 34m²  
výhled do klášterní zahrady, na františkánský klášter a kostel, klidná lokalita

**k dispozici** – manželská postel, lůžkoviny, povlečení, osušky, ručníky, toaletní potřeby, dezinfekce, fén, šatna, posezení, lampičky, ústřední vytápění, chladnička, wifi

**k dispozici** - snídárna s TV, káva, čaj kdykoliv
