---
date: "2018-10-01T00:00:00+02:00"
publishdate: "2018-10-01T00:00:00+02:00"
lastmod: "2018-10-01T00:00:00+02:00"
draft: false
title: "Dvoulůžkový pokoj - Žlutý"
tags: ["okno do ulice", "balkon do dvora", "1. patro", "dvoulůžkový"]
series: ["Pokoje"]
categories: ["Žlutý"]
img: "images/pokoje/zluty.jpg"
toc: true
summary: " "
slug: zluty
weight: -6
---

Rezervovat můžete e-mailem na info@uklasternizahrady.cz

**možnost ubytování pro 1 osobu**  

**1. patro domu, vlastní koupelna**, 25m²  
z koupelny květinový balkón a výhled do atria domu, okno do Valtrovy ulice

**k dispozici** – dle volby manželská&nbsp;postel nebo oddělená lůžka, lůžkoviny, povlečení, osušky, ručníky, toaletní potřeby, dezinfekce, fén, šatna, posezení, lampičky, ústřední vytápění, chladnička, wifi

**k dispozici** - snídárna s TV, káva, čaj kdykoliv
