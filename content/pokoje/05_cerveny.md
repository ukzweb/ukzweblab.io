---
date: "2018-10-01T00:00:00+02:00"
publishdate: "2018-10-01T00:00:00+02:00"
lastmod: "2018-10-01T00:00:00+02:00"
draft: false
title: "Dvoulůžkový pokoj - Červený"
tags: ["okno do ulice", "1. patro", "jednolůžkový"]
series: ["Pokoje"]
categories: ["Červený"]
img: "images/pokoje/cerveny.jpg"
toc: true
summary: " "
slug: cerveny
weight: -5
---

Rezervovat můžete e-mailem na info@uklasternizahrady.cz

**možnost ubytování pro 1 osobu**

**1. patro domu, vlastní koupelna,** 18m²  
výhled do Valtrovy ulice a klášterní zahrady, klidná lokalita

**k dispozici** – dle volby manželská&nbsp;postel nebo oddělená lůžka, lůžkoviny, povlečení, osušky, ručníky, toaletní potřeby, dezinfekce, fén, šatna, posezení, lampičky, ústřední vytápění, chladnička, wifi

**k dispozici** - snídárna s TV, káva, čaj kdykoliv
