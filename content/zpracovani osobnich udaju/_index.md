+++
date = "2018-10-01"
draft = false
title = "Zpracování osobních údajů"

+++

Osobní údaje jsou zpracovávány v souladu s Nařízením Evropského parlamentu a Rady (EU) [2016/679](https://eur-lex.europa.eu/legal-content/CS/TXT/?uri=CELEX:32016R0679) ze dne 27. dubna 2016 o ochraně fyzických osob v souvislosti se zpracováním osobních údajů a o volném pohybu těchto údajů a o zrušení směrnice 95/46/ES (dále jen „Obecné nařízení o ochraně osobních údajů“).

## 1. Jaké údaje zpracováváme
**Identifikační údaje**  
Jméno a příjmení, adresa, IČO (pokud jste podnikatel) a popř. DIČ, název firmy

**Kontaktní údaje**  
E-mailová adresa, telefonní číslo, adresa doručovací nebo fakturační.

## 2. Účely zpracování
K uzavření smlouvy a realizaci Vaší objednávky, ke komunikaci s Vámi, k plnění našich zákonných povinností
Vaše osobní údaje uchováváme jen po tu dobu, dokud trvá  doba zpracování z účelu zpracování anebo je dána právními předpisy.

## 3. Komu poskytujeme údaje
Vaše osobní údaje můžeme poskytnout jiným osobám v souladu s příslušnými právními předpisy.

## 4. Jaká jsou vaše práva
V souvislosti se zpracováním Vašich osobních údajů máte zejména tato práva:

a) právo na jasné a srozumitelné informace o tom, jak používáme Vaše osobní údaje a jaká jsou Vaše práva  
b) právo na přístup k osobním údajům a poskytnutí dalších informací souvisejících s jejich zpracováním  
c) právo na opravu nesprávných a neúplných osobních údajů  
d) právo na vymazání Vašich osobních údajů, především pokud již nejsou dále potřebné pro další zpracování nebo jste odvolali svůj souhlas k jejich zpracování nebo jste oprávněně namítali jejich zpracování nebo byly zpracovány nezákonně nebo musejí být vymazány podle právních předpisů;  
e) právo na omezení zpracování Vašich osobních údajů, pokud napadnete správnost osobních údajů po dobu, dokud neověříme jejich správnost nebo zpracování je protizákonné nebo je už nepotřebujeme  
f) právo získat své osobní údaje a přenést je k jinému poskytovateli služeb  
g) právo podat stížnost na Úřad pro ochranu osobních údajů  
při uplatnění některého z uvedených práv nás kontaktujte

Pro zajištění bezpečnosti Vašich osobních údajů využíváme technická a organizační opatře­ní zejména na ochranu před neoprávněným přístupem k údajům a jejich zneužitím. Kde je to vhodné, využíváme na ochranu Vašich údajů šifrování.

Zpracování osobních údajů probíhá manuálně i v elektronických informačních systémech, které podléhají kontrolám.

Pokud máte dotazy ohledně zpracování Vašich osobních údajů, můžete nás kontaktovat na tel. č. +420728015561 nebo elektronicky na e-mail info@uklasternizahray.cz
