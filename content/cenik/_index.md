+++
date = "2018-12-16"
draft = false
title = "Ceník"

+++


******************************************************************

**Volné termíny zkontrolujte v kalendáři po kliknutí na vybraný pokoj.** 

**NELZE rezervovat on-line**

REZERVUJTE na **info@uklasternizahrady.cz**

*termín, *počet osob, *snídaně

Poptávky vyřizujeme obratem

******************************************************************

**Ubytováváme od 15.00&nbsp; do 18.00 hod**.  
**Vyklizení pokoje** v&nbsp;den odjezdu je **do 10.00&nbsp;hod**.  
Pokud je to v našich možnostech, rádi vyhovíme v potřebné úpravě času příjezdu i odjezdu - vždy po vzájemné dohodě.  


**Parkování** veřejné  
**Uložení KOLA** uvnitř objektu PO DOHODĚ **PŘEDEM**  
**Všechny** prostory domu jsou **NEKUŘÁCKÉ**.  

******************************************************************

V ceně ubytování **NENÍ ZAHRNUT** místní poplatek z pobytu **30,- Kč / 1 os. / 1 noc.** 

**Platba standardně po příjezdu a ubytování**, hotovost, karta, on-line

**předem** převodem na účet dle faktury

********

|**CENY**|pobyt na 2 a více nocí|
|:---|---:|
|**Dvoulůžkový červený** pokoj, dle volby manželská&nbsp;postel nebo oddělená lůžka| CZK&nbsp;1.800,- /1&nbsp;pokoj /1&nbsp;noc |
|**Dvoulůžkový červený** pokoj, rezervace **pro 1 osobu**| CZK&nbsp;1.300,- /1&nbsp;pokoj /1&nbsp;noc|
|**Dvoulůžkový modrý** pokoj, dle volby manželská&nbsp;postel nebo oddělená lůžka| CZK&nbsp;1.900,- /1&nbsp;pokoj /1&nbsp;noc|
|**Dvoulůžkový modrý** pokoj, rezervace **pro 1 osobu**| CZK&nbsp;1.400,- /1&nbsp;pokoj /1&nbsp;noc|
|**Dvoulůžkový zelený** pokoj, manželská&nbsp;postel | CZK&nbsp;2.000,- /1&nbsp;pokoj /1&nbsp;noc|
|**Dvoulůžkový zelený** pokoj, rezervace **pro 1 osobu**| CZK&nbsp;1.500,- /1&nbsp;pokoj /1&nbsp;noc|
|**Dvoulůžkový žlutý** pokoj, dle volby manželská&nbsp;postel nebo oddělená lůžka | CZK&nbsp;1.900,- /1&nbsp;pokoj /1&nbsp;noc|
|**Dvoulůžkový žlutý** pokoj, rezervace **pro 1 osobu**| CZK&nbsp;1.400,- /1&nbsp;pokoj /1&nbsp;noc |
|**Snídaně** | CZK&nbsp;250,-
|**Psy** ani jiné domácí mazlíčky | **Neubytováváme**|

********

|**POBYT POUZE NA 1 NOC**||
|:---|---:|
|**Dvoulůžkový ZELENÝ** pokoj, manželská&nbsp;postel | CZK&nbsp;2.200,- /&nbsp;2 osoby 
|| CZK&nbsp;1.600,- /&nbsp;1 osoba
|**Dvoulůžkový MODRÝ, ŽLUTÝ** pokoj | CZK&nbsp;2.100,- /&nbsp;2 osoby 
|| CZK&nbsp;1.500,- /&nbsp;1 osoba
|**Dvoulůžkový ČERVENÝ** pokoj | CZK&nbsp;2.000,- /&nbsp;2 osoby 
|| CZK&nbsp;1.400,- /&nbsp;1 osoba

********


**DÁRKOVÝ POUKAZ** po dohodě vyhotovíme dle Vašich představ a údajů.  

**Storno poplatky řešíme individuálně**

